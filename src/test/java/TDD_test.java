/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.waritphat.tdd_addandrps.ADD;
import com.waritphat.tdd_addandrps.RPS;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

/**
 *
 * @author domem
 */
public class TDD_test {
    
    public TDD_test() {
    }
    @Test
    public void testAdd_1_1is2(){
        assertEquals(2, ADD.add(1, 1));
    }
    @Test
    public void testAdd_1_2is3(){
        assertEquals(3, ADD.add(1, 2));
    }
    
    //test RPS
    @Test
    public void testRPS_p1_p_p2_p_is_draw(){
        assertEquals("draw", RPS.chup('p', 'p'));
    }
    @Test
    public void testRPS_p1_h_p2_h_is_draw(){
        assertEquals("draw", RPS.chup('h', 'h'));
    }
    @Test
    public void testRPS_p1_r_p2_r_is_draw(){
        assertEquals("draw", RPS.chup('r', 'r'));
    }
    @Test
    public void testRPS_p1_s_p2_p_is_p1(){
        assertEquals("p1", RPS.chup('s', 'p'));
    }
    @Test
    public void testRPS_p1_p_p2_h_is_p1(){
        assertEquals("p1", RPS.chup('p', 'h'));
    }
    @Test
    public void testRPS_p1_h_p2_s_is_p1(){
        assertEquals("p1", RPS.chup('h', 's'));
    }
    @Test
    public void testRPS_p1_p_p2_s_is_p2(){
        assertEquals("p2", RPS.chup('p', 's'));
    }
    @Test
    public void testRPS_p1_h_p2_p_is_p2(){
        assertEquals("p2", RPS.chup('h', 'p'));
    }
    @Test
    public void testRPS_p1_s_p2_h_is_p2(){
        assertEquals("p2", RPS.chup('s', 'h'));
    }    
}
